var prevScrollpos = window.pageYOffset;
window.onscroll = function() {
var currentScrollPos = window.pageYOffset;
  if (prevScrollpos > currentScrollPos) {
    document.getElementById("first-navbar").style.top = "0";
  } else {
    document.getElementById("first-navbar").style.top = "-50px";
  }
  prevScrollpos = currentScrollPos;
  scrollFunction()
}

function scrollFunction() {
  if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
    document.getElementById("second-navbar").style.top = "0"
  } else {
    document.getElementById("second-navbar").style.top = "-100px";
  }
}